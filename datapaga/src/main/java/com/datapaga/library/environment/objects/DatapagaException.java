package com.datapaga.library.environment.objects;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Carlos Cornejo (@icarloscornejo) on 18/12/2017.
 * https://www.linkedin.com/in/icarloscornejo
 */

/**
 * DatapagaException object.
 */
public class DatapagaException implements Parcelable {

    private String error;
    private String serverStatus;

    public DatapagaException(String error, String serverStatus) {
        this.error = error;
        this.serverStatus = serverStatus;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }

    public String getServerStatus() {
        return serverStatus;
    }

    public void setServerStatus(String serverStatus) {
        this.serverStatus = serverStatus;
    }

    @Override
    public String toString() {
        return "DatapagaException{" +
                "error='" + error + '\'' +
                ", serverStatus='" + serverStatus + '\'' +
                '}';
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(this.error);
        dest.writeString(this.serverStatus);
    }

    protected DatapagaException(Parcel in) {
        this.error = in.readString();
        this.serverStatus = in.readString();
    }

    public static final Parcelable.Creator<DatapagaException> CREATOR = new Parcelable.Creator<DatapagaException>() {
        @Override
        public DatapagaException createFromParcel(Parcel source) {
            return new DatapagaException(source);
        }

        @Override
        public DatapagaException[] newArray(int size) {
            return new DatapagaException[size];
        }
    };
}
