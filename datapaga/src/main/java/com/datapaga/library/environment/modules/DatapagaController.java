package com.datapaga.library.environment.modules;

import com.datapaga.library.environment.interfaces.AccountInterface;
import com.datapaga.library.environment.interfaces.CardsInterface;
import com.datapaga.library.environment.interfaces.DatapagaInterface;
import com.datapaga.library.environment.interfaces.TransactionsInterface;

/**
 * Created by Carlos Cornejo (@icarloscornejo) on 7/12/2017.
 * https://www.linkedin.com/in/icarloscornejo
 */

/**
 * The main controller for the Datapaga Instance and the internal modules.
 */
public class DatapagaController implements DatapagaInterface{

    /** The Transactions Interface instance. */
    private TransactionsInterface ti;
    /** The Cards Interface instance. */
    private CardsInterface ci;
    /** The Account Interface instance. */
    private AccountInterface ai;

    /**
     * Datapaga Controller constructor that instantiates all the necesary modules for the Datapaga Library.
     */
    public DatapagaController() {
        this.ti = new TransactionsModule();
        this.ci = new CardsModule();
        this.ai = new AccountModule();
    }

    /**
     * Retrieves the transactions intance provided by DatapagaCore.
     * @return Transaction interface.
     */
    @Override
    public TransactionsInterface transactions() {
        return this.ti;
    }

    /**
     * Retrieves the cards intance provided by DatapagaCore.
     * @return Cards interface.
     */
    @Override
    public CardsInterface cards() {
        return this.ci;
    }

    /**
     * Retrieves the account intance provided by DatapagaCore.
     * @return Account interface.
     */
    @Override
    public AccountInterface account() {
        return this.ai;
    }


}
