package com.datapaga.library.core.api;

import com.android.volley.AuthFailureError;
import com.android.volley.NetworkError;
import com.android.volley.NoConnectionError;
import com.android.volley.ParseError;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.ServerError;
import com.android.volley.TimeoutError;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.datapaga.library.core.DatapagaCore;
import com.datapaga.library.util.JSONBuilder;
import com.google.gson.Gson;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Carlos Cornejo (@icarloscornejo) on 7/12/2017.
 * https://www.linkedin.com/in/icarloscornejo
 */

public abstract class API {

    private static final String BASE_URL_DEV2 = "https://datapaga-staging.herokuapp.com/v1/";
    private static final String BASE_URL_DEV1 = "https://datapaga.herokuapp.com/v1/";
    private static final String BASE_URL_PROD = "https://api.datapaga.com/v1/";

    public static final String BASE_URL = BASE_URL_DEV2;

    protected JSONObject fuseAuth(JSONObject json) {
        JSONBuilder jb;
        if (json != null) {
            jb = new JSONBuilder(json);
        } else {
            jb = new JSONBuilder();
        }
        jb.add("api_key", DatapagaCore.instance().getApiKey());
        jb.add("api_secret", DatapagaCore.instance().getApiSecret());
        return jb.getJSON();
    }

    protected JSONObject getAuth() {
        return fuseAuth(null);
    }

    protected <T> void doRequest(Request<T> req) {
        DatapagaCore.instance().addToRequestQueue(req);
    }

    protected Gson gson() {
        return new Gson();
    }

    private boolean isJSONValid(String str) {
        try {
            new JSONObject(str);
        } catch (JSONException ex) {
            return false;
        }
        return true;
    }

    protected JsonObjectRequest newJSONGETRequest(String endpoint,
                                                  final Response.Listener<JSONObject> listener) {
        return newJSONRequest(Request.Method.GET, endpoint, null, listener);
    }

    protected JsonObjectRequest newJSONPOSTRequest(String endpoint, final JSONObject params,
                                                   final Response.Listener<JSONObject> listener) {
        return newJSONRequest(Request.Method.POST, endpoint, params, listener);
    }

    private JsonObjectRequest newJSONRequest(int requestMethod, String service, final JSONObject params,
                                             final Response.Listener<JSONObject> listener) {
        String URL = BASE_URL + service;
        return new JsonObjectRequest(requestMethod, URL, params,
                listener,
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError err) {
                        try {
                            String errStr = null;

                            if (err instanceof NoConnectionError) {
                                errStr = "NoConnectionError";
                            } else if (err instanceof TimeoutError) {
                                errStr = "TimeoutError";
                            } else if (err instanceof AuthFailureError) {
                                errStr = "AuthFailureError";
                            } else if (err instanceof ServerError) {
                                errStr = "ServerError";
                            } else if (err instanceof NetworkError) {
                                errStr = "NetworkError";
                            } else if (err instanceof ParseError) {
                                errStr = "ParseError";
                            }

                            if(isJSONValid(new String(err.networkResponse.data))){
                                JSONObject errorResponse = new JSONObject(new String(err.networkResponse.data));
                                if (errorResponse.has("error")) {
                                    errStr = errorResponse.getString("error");
                                }
                            }

                            listener.onResponse(
                                    new JSONBuilder()
                                            .keys("error", "serverStatus")
                                            .values(errStr, err.networkResponse != null ? err.networkResponse.statusCode : 0)
                            );
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                }
        );
    }
}
